import * as React from 'react';
import { Text, View,Image,FlatList } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';

import Icon from 'react-native-vector-icons/dist/FontAwesome';

const datas = ["Data1", "Data2", "Data3"]
Icon.loadFont();

function HomeScreen() {
  return (
    <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
      <Text>63010102064</Text>
      <Text>Vittayasak</Text>
      <Text>vittaysak@mfu.ac.th</Text>
    </View>
  );
}

function ListScreen() {
  return (
    <View style={{ flex: 1, }}>
       <Text style={{fontSize:20}}>My Picture</Text>
       <Image
        style={{width:200,height:200,borderRadius:100,alignContent:'center',alignItems:'center'}}
        source={{
          uri: 'https://itschool.mfu.ac.th/fileadmin/_processed_/9/c/csm_%E0%B8%AD.%E0%B8%A7%E0%B8%B4%E0%B8%97%E0%B8%A2%E0%B8%B2%E0%B8%A8%E0%B8%B1%E0%B8%81%E0%B8%94%E0%B8%B4%E0%B9%8C_099c1f17f3.jpg',
        }}
      />
    </View>
  );
}

function BoardScreen() {
  return (
    <View style={{ flex: 1, }}>
       <View style={{flex:1,backgroundColor:'rgba(22,22,200,0.4)'}}><Text>0 Zero</Text></View>
       <View style={{flex:2,backgroundColor:'rgba(22,22,200,0.6)'}}><Text>6 Six</Text></View>
       <View style={{flex:3,backgroundColor:'rgba(22,22,200,0.8)'}}><Text>4 Four</Text></View>

    </View>
  );
}

function SettingsScreen() {
  return (
    <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
      <Text>Settings!</Text>
      <Text>Custom Component Area!</Text>
    </View>
  );
}

const Tab = createBottomTabNavigator();

export default function App() {
  return (
    <NavigationContainer>
      <Tab.Navigator>
        <Tab.Screen name="Tab1" component={HomeScreen} 
          options={{
            tabBarIcon:({color,size}) => (
              <Icon name="home" color={color} size={size} />
            ),
          }}
        />
        <Tab.Screen name="Tab2" component={ListScreen}
         options={{
          tabBarIcon:({color,size}) => (
            <Icon name="list" color={color} size={size} />
          ),
        }} />
        <Tab.Screen name="Tab3" component={BoardScreen} 
         options={{
          tabBarIcon:({color,size}) => (
            <Icon name="edit" color={color} size={size} />
          ),
        }}/>
        {/* <Tab.Screen name="Settings" component={SettingsScreen}
         options={{
          tabBarIcon:({color,size}) => (
            <Icon name="gear" color={color} size={size} />
          ),
        }} /> */}
      </Tab.Navigator>
    </NavigationContainer>
  );
}